#!/usr/bin/env groovy

// Compiles JUnit with ExtendJ and runs all tests.
// The Maven build has to run in a Java 6 VM.
// Maven 3.2.5 is the latest Maven version that runs on Java 6.
// We can't use Artifactory for this build because it does not work on Java 6.
// Therefore, we just rely on the local maven repos to get artifacts from upstream builds.

node {
	env.JAVA_HOME="${tool 'oracle-jdk-6'}"
	env.MAVEN_HOME="${tool 'maven-3.2.5'}"
	env.PATH="${env.JAVA_HOME}/bin:${env.MAVEN_HOME}/bin:${env.PATH}"

	stage ('Checkout') {
		checkout scm
	}

	stage ('Build') {
		sh 'mvn --batch-mode -Dmaven.test.failure.ignore=true clean test'
		junit 'target/surefire-reports/**/*.xml'
	}
}
